import React from "react";
import { useSelector } from "react-redux";
import CartItems from "./CartItems";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Col, Container, Row } from "react-bootstrap";

interface CartItem {
  id: string;
  title: string;
  price: number;
  quantity: number;
  image: string;
}

interface CartState {
  items: CartItem[];
  totalAmount: number;
}

const Cart: React.FC = () => {
  const cart: CartState = useSelector((state: any) => state.cart);

  let content: React.ReactNode;

  if (cart.items.length) {
    content = (
      <Container className="mb-5">
        {cart.items.map((cartItem) => (
          <CartItems key={cartItem.id} {...cartItem} />
        ))}
        <Row className="mt-5">
       
          <Col xs={12} md={6}>
         
            <Card>
              
              <Card.Body>
                <Card.Title>Your Total Amount is:</Card.Title>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={6}>
            {" "}
           
            <Card>
              
              <Card.Body>
                <Card.Title>${cart.totalAmount}</Card.Title>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  } else {
    content = (
      <Container>
    Cart is Empty
      </Container>
    );
  }

  return <>{content}</>;
};

export default Cart;
