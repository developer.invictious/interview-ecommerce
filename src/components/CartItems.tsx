import React from "react";
import { useDispatch } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";

import {
  addItemToCart,
  removeItemToCart,
} from "../reducer/features/cart-slice";
import { Button, Card, Col, Row } from "react-bootstrap";

interface CartItemProps {
  id: string;
  image: string;
  title: string;
  quantity: number;
  price: number;
  data?: any;
}

const CartItem: React.FC<CartItemProps> = ({
  id,
  image,
  title,
  quantity,
  price,
  data,
}) => {
  const dispatch = useDispatch();

  const addItemHandler = () => {
    dispatch(addItemToCart({ ...data }));
  };

  const removeItemHandler = () => {
    dispatch(removeItemToCart(id));
  };

  return (
    <Card className="mb-3">
      
      <Row>
        
        <Col md={3}>
          
          <img src={image} alt={title} className="img-fluid" />
         
        </Col>
        <Col md={6} className="d-flex flex-column justify-content-between">
         
         
          <div>
            <h5 className="card-title">{title}</h5>
            <p className="card-text">
              Price: {quantity} x ${price}
            </p>
          </div>
        </Col>
        <Col md={3} className="d-flex justify-content-end align-items-center">
          
          <Button variant="primary" className="me-2" onClick={addItemHandler}>
          <i className="bi bi-plus"></i>
           
          </Button>
          <Button variant="danger" onClick={removeItemHandler}>
          <i className="bi bi-file-earmark-x"></i>
          </Button>
        </Col>
      </Row>
    </Card>
  );
};

export default CartItem;
