import React from "react";

import { Link, Outlet } from "react-router-dom";
import { useSelector } from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Badge, Button, Container, FormControl, InputGroup, Nav, NavItem, Navbar } from "react-bootstrap";


const NavBar = () => {
  const totalQuantity = useSelector((state: any) => state.cart.totalQuantity);
  return (
    <>
       <Navbar bg="light" expand="lg" >
        <Container fluid>
          <Link className="navbar-brand" to="/">ECOMMERCE</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <Nav className="me-auto mb-2 mb-lg-0">
              <NavItem>
                <InputGroup className="mb-3">
                  <InputGroup.Text className="bg-light">
                  <i className="bi bi-search"></i>
                  </InputGroup.Text>
                  <FormControl className="border border-dark" placeholder="Search" />
                </InputGroup>
              </NavItem>
            </Nav>
            <Nav>
              <NavItem>
                <Button variant="outline-primary">Login</Button>
              </NavItem>
              <NavItem>
                <Button variant="primary">Register</Button>
              </NavItem>
              <NavItem>
                <Link className="nav-link" to="/cart">
                  <Badge bg="primary" text="white">
                  <i className="bi bi-bag"></i> {totalQuantity}
                  </Badge>
                </Link>
              </NavItem>
            </Nav>
          </div>
        </Container>
      </Navbar>
      <Outlet />
    </>
  );
};

export default NavBar;

