import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { useParams } from "react-router-dom";
import {
  fetchSingleProduct,
  removeData,
} from "../reducer/features/single-productSlice";
import { useSelector } from "react-redux";
import Spinner from "./Spinner";
import { addItemToCart } from "../reducer/features/cart-slice";
import 'bootstrap/dist/css/bootstrap.min.css';



const ProductDetail = () => {
  const { productId } = useParams<{ productId: any }>();
  const dispatch = useDispatch<any>();
  const product = useSelector((state: any) => state.singleProduct.product);
  const fetchStatus = useSelector((state: any) => state.singleProduct.status);
  const error = useSelector((state: any) => state.singleProduct.error);

  const addItemHandler = () => {
    dispatch(addItemToCart(product));
  };

  useEffect(() => {
    dispatch(fetchSingleProduct(productId));

    return () => dispatch(removeData());
  }, [productId, dispatch]);

  let content;

  if (fetchStatus === "loading") {
    return (
     
        <Spinner />
      
    );
  }

  if (fetchStatus === "succeeded") {
    return (<div className="container mb-3">  
      <div className="row">  
        <div className="col-md-6">  
          <img src={product.image} className="img-fluid" alt={product.title} />
        </div>
        <div className="col-md-6">  
          <h5 className="card-title">{product.title}</h5>
          <p className="card-text">{product.description}</p>
          <p className="card-text">Price: ${product.price}</p>
          <button className="btn btn-primary" onClick={addItemHandler}>
            Add To Cart
          </button>
        </div>
      </div>
    </div>
    );
  }

  if (fetchStatus === "failed") {
    content = <div>{error}</div>;
  }

  return <>{content}</>;
};

export default ProductDetail;

