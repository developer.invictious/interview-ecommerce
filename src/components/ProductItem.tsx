import React from "react";

import { Link } from "react-router-dom";
import productsReducer from "../reducer/features/products-slice";
import "bootstrap/dist/css/bootstrap.min.css";

interface Product {
  product: any;
}

const ProductItem: React.FC<any> = ({ products }) => {
  const modifyText = (text: string, size: number): string =>
    text.length > size ? text.split("").splice(0, size).join("") + "..." : text;

  return (
    <>
      {products.map((product: any) => (
        <div className="card mb-3" key={product.id}>
        <img src={product.image} className="card-img-top" alt={product.title} />
        <div className="card-body">
          <h5 className="card-title">{modifyText(product.title, 20)}</h5>
          <p className="card-text">{modifyText(product.description, 100)}</p>
          <div className="d-flex justify-content-between align-items-center">
            <p className="card-text mb-0">Price: ${product.price}</p>
            <Link to={`/product/${product.id}`} className="btn btn-primary">
              See Details
            </Link>
          </div>
        </div>
      </div>
      ))}
    </>
  );
};

export default ProductItem;
