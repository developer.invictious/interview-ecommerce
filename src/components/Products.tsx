import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchProducts } from "../reducer/features/products-slice";
import ProductItem from "./ProductItem";
import Spinner from "./Spinner";
import { Container } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

interface Product {
  id: string;
  name: string;
  category: string;
}

const Products: React.FC = () => {
  const [selectFilter, setSelectFilter] = useState<string>("All");
  const products: Product[] = useSelector(
    (state: any) => state.products.products
  );
  const dispatch = useDispatch<any>();
  const fetchStatus: string = useSelector(
    (state: any) => state.products.status
  );
  const error: string = useSelector((state: any) => state.products.error);

  let productData: Product[] = [...products];

  if (selectFilter !== "All") {
    productData = products.filter(
      (product: Product) => product.category === selectFilter
    );
  }

  const uniqueCategory: string[] = [
    "All",
    ...new Set(products.map((product: Product) => product.category)),
  ];

  useEffect(() => {
    if (fetchStatus === "idle") {
      dispatch(fetchProducts());
    }
  }, [dispatch, fetchStatus]);

  const selectChangeHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectFilter(e.target.value);
  };

  let content;

  if (fetchStatus === "loading") {
    return (
      <Container>
        <Spinner />
      </Container>
    );
  }
  if (fetchStatus === "succeeded") {
    return (
       <div className="container">
       <div className="row mb-3">
         <div className="col-md-8">
           <h1>Products</h1>
         </div>
         <div className="col-md-4">
           <select className="form-control" value={selectFilter} onChange={selectChangeHandler}>
             <option value="">All Categories</option>
             {uniqueCategory.map((unique) => (
               <option key={unique} value={unique}>
                 {unique}
               </option>
             ))}
           </select>
         </div>
       </div>
       <div className="row">
         <ProductItem products={products} />
       </div>
     </div>
    );
  }
  if (fetchStatus === "failed") {
    content = <Container>{error}</Container>;
  }

  return <>{content}</>;
};

export default Products;
