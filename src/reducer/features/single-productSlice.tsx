import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ProductState {
  product: Record<string, any>;
  status: "idle" | "loading" | "succeeded" | "failed";
  error: string | null;
}

const initialState: ProductState = {
  product: {},
  status: "idle",
  error: null,
};

export const fetchSingleProduct = createAsyncThunk(
  "fetchSingleProduct",
  async (id: number) => {
    const response = await fetch(`https://fakestoreapi.com/products/${id}`);
    const data = await response.json();
    return data;
  }
);

const singleProductSlice = createSlice({
  name: "singleProduct",
  initialState,
  reducers: {
    removeData(state) {
      state.product = {};
    },
  },

  extraReducers(builder) {
    builder
      .addCase(fetchSingleProduct.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchSingleProduct.fulfilled, (state, action: PayloadAction<any>) => {
        state.status = "succeeded";
        state.product = { ...action.payload };
      })
      .addCase(fetchSingleProduct.rejected, (state, action: PayloadAction<any>, ) => {
        state.status = "failed";
        
      });
  },
});

const singleProductReducer = singleProductSlice.reducer;
export const { removeData } = singleProductSlice.actions;
export default singleProductReducer;