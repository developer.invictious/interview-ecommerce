import { configureStore, combineReducers } from "@reduxjs/toolkit";
import cartReducer from "./reducer/features/cart-slice";
import productsReducer from "./reducer/features/products-slice";
import singleProductReducer from "./reducer/features/single-productSlice";

const rootReducer = combineReducers({
  products: productsReducer,
  singleProduct: singleProductReducer,
  cart: cartReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;